import 'package:flutter/material.dart';

class AppColors {
  Color _hex({required String colorCode}) {
    final String containHex = colorCode.toUpperCase().replaceAll('#', '');
    String result = '';
    if (colorCode.length == 7) {
      result = 'FF$containHex';
    }

    return Color(int.parse(result, radix: 16));
  }
  //list untuk hex color(Custom Color)
  static Color orangebar = AppColors()._hex(colorCode: '#FF8963');
  static Color homebackground = AppColors()._hex(colorCode: '#FAFAFA');
  static Color navbar = AppColors()._hex(colorCode: '#FFFFFF');
  static Color font = AppColors()._hex(colorCode: '#121F56');
  static Color purple = AppColors()._hex(colorCode: '#ECD6FD');
  static Color black = AppColors()._hex(colorCode: '#000000');
  static Color greydisabled= AppColors()._hex(colorCode: '#DADADA');
  static Color grey= AppColors()._hex(colorCode: '#D3D3D3');


  
  
  
  }