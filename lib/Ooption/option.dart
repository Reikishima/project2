import 'package:flutter/material.dart';
import 'package:project2/Utils/appcolors.dart';



class Option extends StatelessWidget {
 
  const Option({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {},
              icon: Image.asset('assets/images/park.jpg'),
            ),
          ],
          backgroundColor: AppColors.purple,
          leading: Icon(
            Icons.arrow_back_ios_rounded,
            color: AppColors.font,
          ),
          shadowColor: AppColors.orangebar,
          title: Text(
            'ListView',
            style: TextStyle(
                color: AppColors.font,
                fontFamily: 'Times New Roman',
                fontWeight: FontWeight.w300),
          ),
        ),
        body: Container(
          color: AppColors.homebackground,
          child:
              list(context),
        ));
  }
}

/// Widget ListView
Widget list(BuildContext context) {
  return ListView(
    scrollDirection: Axis.vertical,
    children: <Widget>[
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.orangebar,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled,fontFamily: 'Calibri'))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.orangebar,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.orangebar,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.orangebar,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
              text: 'Ahmad\n',
              style: TextStyle(color: AppColors.font),
              children: <TextSpan>[
                TextSpan(
                    text: 'sudah selesai belum?',
                    style: TextStyle(color: AppColors.greydisabled))
              ]),
        ),
      ),
      ListTile(
        leading: Icon(
          Icons.person,
          size: 40,
          color: AppColors.greydisabled,
        ),
        title: RichText(
          text: TextSpan(
            text: 'Ahmad\n',
            style: TextStyle(color: AppColors.font),
            children: <TextSpan>[
              TextSpan(
                  text: 'sudah selesai belum?',
                  style: TextStyle(color: AppColors.greydisabled))
            ],
          ),
        ),
      ),
    ],
  );
}

