import 'package:flutter/material.dart';
import 'package:project2/Utils/appcolors.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../Utils/sizeconfig.dart';

class Location extends StatelessWidget {
  const Location({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: AppColors.homebackground,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: SizeConfig.vertical(2.5)),
              child: Container(
                width: SizeConfig.screenWidth,
                height: SizeConfig.vertical(50),
                decoration: BoxDecoration(
                    image: const DecorationImage(
                        image: AssetImage('assets/images/park.jpg'),
                        fit: BoxFit.cover),
                    color: AppColors.grey),
                child: ResponsiveRowColumn(
                  layout: ResponsiveRowColumnType.COLUMN,
                  children: <ResponsiveRowColumnItem>[
                    ResponsiveRowColumnItem(child: row2(context))
                  ],
                ),
              ),
            ),
            ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: _button1(context))
              ],
            ),
            ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: _button2(context))
              ],
            ),
            ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: _button3(context))
              ],
            ),
            // GridView.count(
            //     crossAxisCount: 2,
            //     mainAxisSpacing: 10,
            //     crossAxisSpacing: 10,
            //     scrollDirection: Axis.vertical
            //     ,children:<Widget>[
            //       Container(
            //      padding: EdgeInsets.only(left: SizeConfig.horizontal(3),
            //      top: SizeConfig.vertical(2.5)),
            //      color: AppColors.black, 
            //      child: _button2(context),   
            // )
            //     ]),
          ],
        ),
      ),
    );
  }
}

///
Widget row2(BuildContext context) {
  return Row(children: <Widget>[
    TextButton.icon(
        onPressed: () {},
        icon: Icon(
          Icons.location_on,
          color: AppColors.orangebar,
        ),
        label: Text(
          'Your Parking',
          style: TextStyle(
              color: AppColors.font, fontSize: 18, fontWeight: FontWeight.w700),
        ))
  ]);
}

Widget _button1(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      SizedBox(
        width: SizeConfig.horizontal(45),
        height: SizeConfig.vertical(10),
        child: TextButton(
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(width: 1, color: AppColors.orangebar),
            ),
          ),
          onPressed: () {},
          child: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  bottom: SizeConfig.vertical(1.5),
                  top: SizeConfig.vertical(2),
                ),
                child: const CircleAvatar(
                  radius: 30,
                  backgroundImage: AssetImage('assets/images/images.png'),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                      bottom: SizeConfig.vertical(1.5),
                    ),
                    child: Text(
                      'Micro',
                      style: TextStyle(
                          color: AppColors.font,
                          fontSize: 16,
                          fontWeight: FontWeight.w800),
                    ),
                  ),
                  Text(
                    '17 Places',
                    style: TextStyle(color: AppColors.grey),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
      SizedBox(
        width: SizeConfig.horizontal(45),
        height: SizeConfig.vertical(10),
        child: TextButton(
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(width: 1, color: AppColors.grey),
            ),
          ),
          onPressed: () {},
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  bottom: SizeConfig.vertical(1.5),
                  top: SizeConfig.vertical(2),
                ),
                child: const CircleAvatar(
                  radius: 30,
                  backgroundImage: AssetImage('assets/images/images.png'),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: SizeConfig.vertical(1.5)),
                    child: Text(
                      'Sedan',
                      style: TextStyle(
                          color: AppColors.font,
                          fontSize: 16,
                          fontWeight: FontWeight.w800),
                    ),
                  ),
                  Text(
                    '8 Places',
                    style: TextStyle(color: AppColors.grey),
                  )
                ],
              ),
            ],
          ),
        ),
      )
    ],
  );
}

///Widget _button2
Widget _button2(BuildContext context) {
  return Row(
    children: [
      Padding(
        padding: EdgeInsets.only(
            left: SizeConfig.horizontal(3.5), top: SizeConfig.vertical(2)),
        child: SizedBox(
          width: SizeConfig.horizontal(45),
          height: SizeConfig.vertical(10),
          child: TextButton(
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
                side: BorderSide(width: 1, color: AppColors.greydisabled),
              ),
            ),
            onPressed: () {},
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    bottom: SizeConfig.vertical(1.5),
                    top: SizeConfig.vertical(2),
                  ),
                  child: const CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage('assets/images/images.png'),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                        bottom: SizeConfig.vertical(1.5),
                      ),
                      child: Text(
                        'SUV',
                        style: TextStyle(
                            color: AppColors.font,
                            fontSize: 16,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                    Text(
                      '24 Places',
                      style: TextStyle(color: AppColors.grey),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
            left: SizeConfig.horizontal(3.5), top: SizeConfig.vertical(2)),
        child: SizedBox(
          width: SizeConfig.horizontal(45),
          height: SizeConfig.vertical(10),
          child: TextButton(
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
                side: BorderSide(width: 1, color: AppColors.greydisabled),
              ),
            ),
            onPressed: () {},
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    bottom: SizeConfig.vertical(1.5),
                    top: SizeConfig.vertical(2),
                  ),
                  child: const CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage('assets/images/images.png'),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                        bottom: SizeConfig.vertical(1.5),
                      ),
                      child: Text(
                        'Van',
                        style: TextStyle(
                            color: AppColors.font,
                            fontSize: 16,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                    Text(
                      '5 Places',
                      style: TextStyle(color: AppColors.grey),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    ],
  );
}

/// Widget Button3 orange
Widget _button3(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(top: SizeConfig.vertical(2.5) ),
    child: SizedBox(
      width: SizeConfig.horizontal(93),
      child: ElevatedButton.icon(
        style: ElevatedButton.styleFrom(backgroundColor: AppColors.orangebar),
        onPressed: () {},
        icon: const Icon(Icons.location_on),
        label: Text(
          'Plan Parking Now',
          style: TextStyle(color: AppColors.homebackground),
        ),
      ),
    ),
  );
}
