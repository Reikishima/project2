import 'package:flutter/material.dart';
import 'package:project2/Utils/appcolors.dart';
import 'package:project2/Utils/sizeconfig.dart';
import 'package:responsive_framework/responsive_framework.dart';

void main() {
  runApp(const Home());
}

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: AppColors.homebackground,
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              Container(
                width: SizeConfig.screenWidth,
                height: SizeConfig.vertical(36.5),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          'assets/images/download.png',
                        ),
                        fit: BoxFit.cover)),
                padding: EdgeInsets.only(
                  left: SizeConfig.horizontal(0),
                  top: SizeConfig.horizontal(0.5),
                ),
                child: ResponsiveRowColumn(
                  rowCrossAxisAlignment: CrossAxisAlignment.start,
                  layout: ResponsiveRowColumnType.COLUMN,
                  children: <ResponsiveRowColumnItem>[
                    ResponsiveRowColumnItem(
                      child: rows(context),
                    ),
                    ResponsiveRowColumnItem(
                      child: columns(context),
                    ),
                    ResponsiveRowColumnItem(
                      child: rows2(context),
                    ),
                  ],
                ),
              ),
              // GridView.count(
              //   crossAxisCount: 2,
              //   // scrollDirection: Axis.vertical,
              //   children: <Widget>[
              //     _button(context),
              //   ],),
              Padding(
                padding: EdgeInsets.only(
                  top: SizeConfig.vertical(2),
                ),
                child: SizedBox(
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.vertical(23.5),
                  child: ResponsiveRowColumn(
                    layout: ResponsiveRowColumnType.COLUMN,
                    children: <ResponsiveRowColumnItem>[
                      ResponsiveRowColumnItem(
                        child: _button(context),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth,
                height: SizeConfig.vertical(30),
                child: Container(
                  height: SizeConfig.vertical(30),
                  color: AppColors.greydisabled,
                  child: ResponsiveRowColumn(
                    layout: ResponsiveRowColumnType.COLUMN,
                    children: <ResponsiveRowColumnItem>[
                      ResponsiveRowColumnItem(
                        child: _location(context),
                      ),
                      ResponsiveRowColumnItem(
                        child: price(context),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

/// digunakan untuk me-row bagian atas cuaca dan more_horiz
Widget rows(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      TextButton.icon(
        label: Text(
          '24°C',
          style: TextStyle(
              color: AppColors.font, fontWeight: FontWeight.bold, fontSize: 20),
        ),
        onPressed: () {},
        icon: Image.asset(
          'assets/images/download.jpg',
          width: SizeConfig.safeVertical(6),
          height: SizeConfig.safeHorizontal(6),
        ),
      ),
      IconButton(
        onPressed: () {},
        icon: Icon(
          Icons.more_horiz,
          color: AppColors.font,
        ),
      )
    ],
  );
}

/// Membuat sebuah Tulisan Column
Widget columns(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          right: SizeConfig.horizontal(93),
          bottom: SizeConfig.vertical(1.5),
        ),
      ),
      Text(
        'Hi, Iquas',
        style: TextStyle(
            color: AppColors.font, fontSize: 18, fontWeight: FontWeight.w400),
      ),
      Padding(
        padding: EdgeInsets.only(
          bottom: SizeConfig.vertical(1.5),
        ),
      ),
      Text(
        'Find the Best \nVehicle & Parking',
        style: TextStyle(
            color: AppColors.font, fontSize: 22, fontWeight: FontWeight.bold),
      ),
      Padding(
        padding: EdgeInsets.only(
          bottom: SizeConfig.vertical(1.5),
        ),
      ),
    ],
  );
}

/// membuat sebuah row textfield dan icon hamburger
Widget rows2(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          right: SizeConfig.horizontal(9),
        ),
        child: SizedBox(
          width: SizeConfig.horizontal(70),
          child: TextField(
            decoration: InputDecoration(
                hintText: 'Find Places ..',
                hintStyle: TextStyle(color: AppColors.font, fontSize: 13),
                prefixIcon: Icon(
                  Icons.search,
                  color: AppColors.orangebar,
                ),
                filled: true,
                fillColor: AppColors.navbar,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide.none,
                )),
          ),
        ),
      ),
      SizedBox(
        height: SizeConfig.vertical(7.5),
        width: SizeConfig.horizontal(13.5),
        child: Container(
          height: SizeConfig.vertical(10),
          decoration: BoxDecoration(
            color: AppColors.orangebar,
            borderRadius: BorderRadius.circular(10),
          ),
          child: IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.dehaze,
              color: AppColors.navbar,
            ),
          ),
        ),
      )
    ],
  );
}

/// membuat sebuah widget button dengan row
Widget _button(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      SizedBox(
        width: SizeConfig.horizontal(40),
        height: SizeConfig.vertical(22),
        child: TextButton(
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(width: 1, color: AppColors.greydisabled),
            ),
          ),
          onPressed: () {},
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  bottom: SizeConfig.vertical(1.5),
                  top: SizeConfig.vertical(1),
                ),
                child: const CircleAvatar(
                  radius: 30,
                  backgroundImage: AssetImage('assets/images/images.png'),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  bottom: SizeConfig.vertical(1.5),
                ),
                child: Text(
                  'Rent Parking',
                  style: TextStyle(
                      color: AppColors.font,
                      fontSize: 16,
                      fontWeight: FontWeight.w800),
                ),
              ),
              Text(
                '74 Spaces',
                style: TextStyle(color: AppColors.grey),
              )
            ],
          ),
        ),
      ),
      SizedBox(
        width: SizeConfig.horizontal(40),
        height: SizeConfig.vertical(22),
        child: TextButton(
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(width: 1, color: AppColors.orangebar),
            ),
          ),
          onPressed: () {},
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  bottom: SizeConfig.vertical(1.5),
                  top: SizeConfig.vertical(1.5),
                ),
                child: const CircleAvatar(
                  radius: 30,
                  backgroundImage: AssetImage('assets/images/images.png'),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: SizeConfig.vertical(1.1)),
                child: Text(
                  'Rent Vehicle',
                  style: TextStyle(
                      color: AppColors.font,
                      fontSize: 16,
                      fontWeight: FontWeight.w800),
                ),
              ),
              Text(
                '74 available',
                style: TextStyle(color: AppColors.grey),
              )
            ],
          ),
        ),
      )
    ],
  );
}

/// Widget untuk membuat Row select location
Widget _location(BuildContext context) {
  return Row(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
            top: SizeConfig.vertical(2.5), left: SizeConfig.horizontal(5)),
        child: Text(
          'Select Location',
          style: TextStyle(
              color: AppColors.font, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
    ],
  );
}

Widget price(BuildContext context) {
  return Row(
    children: <Widget>[
      Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
                top: SizeConfig.vertical(2.5), left: SizeConfig.horizontal(5)),
            child: SizedBox(
              width: SizeConfig.horizontal(90),
              height: SizeConfig.vertical(10),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: AppColors.homebackground),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: SizeConfig.vertical(3)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'From',
                            style: TextStyle(color: AppColors.font),
                          ),
                          Text(
                            'London',
                            style: TextStyle(color: AppColors.font),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: SizeConfig.horizontal(7)),
                      child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.change_circle,
                            color: AppColors.orangebar,
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.vertical(3),
                          left: SizeConfig.horizontal(3)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'To',
                            style: TextStyle(color: AppColors.font),
                          ),
                          Text(
                            'Manchester',
                            style: TextStyle(color: AppColors.font),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: SizeConfig.vertical(1), left: SizeConfig.horizontal(5)),
            child: SizedBox(
              width: SizeConfig.horizontal(90),
              height: SizeConfig.vertical(7),
              child: Container(
                width: SizeConfig.horizontal(2.5),
                height: SizeConfig.vertical(7),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: AppColors.homebackground),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.only(left: SizeConfig.horizontal(2.5)),
                      child: Text(
                        'Cost Estimate',
                        style: TextStyle(color: AppColors.grey),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(right: SizeConfig.horizontal(2.5)),
                      child: Text(
                        "\$39,00",
                        style: TextStyle(
                            color: AppColors.font,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ],
  );
}
