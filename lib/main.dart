import 'package:flutter/material.dart';
import 'package:project2/Home/home.dart';
import 'package:project2/Utils/appcolors.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'Files/files.dart';

import 'Location/location.dart';
import 'Ooption/option.dart';

void main() {
  runApp(const MyProject());
}
class MyProject extends StatefulWidget {
  const MyProject({super.key});

  @override
  State<MyProject> createState() => _MyProjectState();
}

class _MyProjectState extends State<MyProject> {
  //initiate variable for move state on navbar
  int _selectedIndex = 0;
  static List<Widget> navbar = <Widget>[
    const Home(),
    const Location(),
     const Files(),
     Option()
  ];
  //use to move state on navbar
  void _ontappedIndex(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: AppColors.homebackground,
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: AppColors.navbar,
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home_outlined,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.location_on_outlined, 
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.description_outlined,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
              ),
              label: '',
            ),
          ],unselectedItemColor: AppColors.grey,
          selectedItemColor: AppColors.orangebar,
          currentIndex: _selectedIndex,
          onTap: _ontappedIndex,
        ),
        body: Center(
          child: navbar.elementAt(_selectedIndex),
        ),
      ),
      builder: (BuildContext context, widget) => ResponsiveWrapper.builder(
        ClampingScrollWrapper.builder(context, widget!),
        minWidth: 375,
        maxWidth: 812,
        breakpoints: const <ResponsiveBreakpoint>[
          ResponsiveBreakpoint.autoScale(375, name: MOBILE),
        ],
      ),
    );
  }
}



  /*@override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}*/
